#!/usr/bin/python3
# encoding=utf-8
#
# Copyright © 2015 Alexandre Detiste <alexandre@detiste.be>
# SPDX-License-Identifier: GPL-2.0-or-later

import os
import subprocess
import sys
import time

#BASE = '/home/tchet/git'
#GDP = os.path.join(BASE, 'game-data-packager')

GDP = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
BASE = os.path.dirname(GDP)

subprocess.check_call(['rm', '-rvf', 'ref.zip'],
                       cwd = GDP)
subprocess.check_call(['git', 'checkout', 'debian/changelog'],
                       cwd = GDP)
subprocess.check_call(['git', 'checkout', 'debian/control'],
                       cwd = GDP)

from debian.changelog import Changelog
with open('debian/changelog', encoding='utf-8') as log:
    cl = Changelog(log, strict=False)

assert cl.distributions in ('unstable','UNRELEASED'), cl.distributions

if cl.distributions == 'unstable':
   build_type = 'Backport to PPA'
   build_number = 'release+'
else:
   build_type = 'Git snapshot'
   build_number = time.strftime('git%Y%m%d+')

with open('/usr/share/distro-info/ubuntu.csv', 'r') as data:
    for line in data.readlines():
       version, _, release, _, date, _ = line.split(',', 5)
       if version == 'version':
          continue
       if len(sys.argv) > 1 and sys.argv[1] == release:
          current = release
          lts = release
          break
       if time.strptime(date, '%Y-%m-%d') > time.localtime():
          continue
       current = release
       if 'LTS' in version:
          lts = release

releases = sorted(set([lts, current]))
print('RELEASES:', releases)

with open('debian/control', 'r') as compat:
    for line in compat:
        if 'debhelper-compat' in line:
            current_debhelper =  int(line.split('(')[1].strip(' =),\n'))
            break

for release in sorted(releases):

    supported_debhelper = {
            'xenial': 9,
            }.get(release, current_debhelper)
    BACKPORT = supported_debhelper < current_debhelper

    if BACKPORT:
        build_dep = 'debhelper-compat ( = %d)' %  supported_debhelper
        subprocess.check_call(['sed', '-i',
                               's/\ *debhelper-compat.*/ ' + build_dep + ',/',
                               'debian/control'],
                              cwd = GDP)

    snapshot = str(cl.version) + '~' + build_number + release
    subprocess.check_call(['dch', '-b',
                           '-D', release,
                           '-v', snapshot,
                           build_type],
                          cwd = GDP)

    subprocess.check_call(['debuild', '-S', '-i'],cwd = GDP)
    subprocess.check_call(['dput', 'my-ppa',
                           'game-data-packager_%s_source.changes' % snapshot],
                           cwd = BASE)

    subprocess.check_call(['git', 'checkout', 'debian/changelog'],
                          cwd = GDP)
    if BACKPORT:
        subprocess.check_call(['git', 'checkout', 'debian/control'],
                               cwd = GDP)

    for file in ('.tar.xz',
                 '.dsc',
                 '_source.build',
                 '_source.changes',
                 '_source.my-ppa.upload'):
        subprocess.check_call(['rm', '-v',
                               'game-data-packager_%s%s' % (snapshot, file)],
                              cwd = BASE)
