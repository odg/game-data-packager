#!/usr/bin/python3
# encoding=utf-8
#
# Copyright © 2014 Simon McVittie <smcv@debian.org>
# SPDX-License-Identifier: GPL-2.0-or-later

import os
import sys
import yaml

if 'GDP_UNINSTALLED' not in os.environ:
    sys.path.insert(0, '/usr/share/game-data-packager')
    sys.path.insert(0, '/usr/share/games/game-data-packager')

from game_data_packager.game import load_games
from game_data_packager.util import ascii_safe

if __name__ == '__main__':
    for name, game in load_games().items():

        try:
            game.load_file_data()
        except Exception:
            sys.stderr.write('While loading %s:\n' % game)
            raise

        ascii_safe(game.longname, force=True).encode('ascii')
        ascii_safe(game.help_text, force=True).encode('ascii')
        if 'DEBUG' in os.environ or 'GDP_DEBUG' in os.environ:
            print('# %s -----------------------------------------' % name)
            print(yaml.safe_dump(game.to_data()))
