#!/usr/bin/python3
# encoding=utf-8
#
# Copyright © 2016 Simon McVittie <smcv@debian.org>
# SPDX-License-Identifier: GPL-2.0-or-later

import os
import sys

from game_data_packager.packaging import (get_native_packaging_system)

def main(f, out):
    data = open(f, encoding='utf-8').read()
    data = get_native_packaging_system().substitute(data,
            'unknown-package-name')
    open(out + '.tmp', 'w', encoding='utf-8').write(data)
    os.rename(out + '.tmp', out)

if __name__ == '__main__':
    main(sys.argv[1], sys.argv[2])

