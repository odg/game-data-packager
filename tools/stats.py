#!/usr/bin/python3
# encoding=utf-8
#
# Copyright © 2015 Alexandre Detiste <alexandre@detiste.be>
# SPDX-License-Identifier: GPL-2.0-or-later

from game_data_packager.game import (load_games)

games = []
order = { 'demo' : 1,
          'full' : 2,
          'expansion' : 3}
for name, game in load_games().items():
    game.load_file_data()
    for package in game.packages.values():
        if package.rip_cd:
            continue
        size_min, size_max = game.size(package)
        games.append({
             'game': name,
             'year': int((package.copyright or game.copyright)[2:6]),
             'type': package.type,
             'fanmade': {True: 'Y'}.get(game.fanmade, 'N'),
             'package': package.name,
             'disks': package.disks or game.disks or 1,
             'engine': package.engine or game.engine or '',
             'size_min': size_min,
             'size_max': size_max,
             })

games = sorted(games, key=lambda k: (k['game'], order[k['type']], k['package']))

print('GAME;YEAR;TYPE;FANMADE;PACKAGE;DISKS;ENGINE;SIZE_MIN;SIZE_MAX')
for g in games:
   print('%s;%d;%s;%s;%s;%d;%s;%d;%d' % (g['game'], g['year'], g['type'], g['fanmade'],
                                      g['package'], g['disks'], g['engine'], g['size_min'], g['size_max']))
