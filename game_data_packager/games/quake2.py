#!/usr/bin/python3
# encoding=utf-8
#
# Copyright © 2015 Simon McVittie <smcv@debian.org>
# SPDX-License-Identifier: GPL-2.0-or-later

import glob
import logging
import os
import subprocess
import tarfile

from ..build import (PackagingTask)
from ..game import (GameData)

logger = logging.getLogger(__name__)

class Quake2GameData(GameData):
    def construct_task(self, **kwargs):
        return Quake2Task(self, **kwargs)

class Quake2Task(PackagingTask):
    def fill_extra_files(self, package, destdir):
        super(Quake2Task, self).fill_extra_files(package, destdir)

        if package.name not in ('quake2-reckoning-data',
                                'quake2-groundzero-data'):
            return

        subdir = {
            'quake2-groundzero-data': 'rogue',
            'quake2-reckoning-data': 'xatrix',
        }[package.name]

        installdir = os.path.join(destdir, 'usr', 'share', 'games', 'quake2')
        unpackdir = os.path.join(self.get_workdir(), 'tmp',
                package.name + '.build.d')

        tars = list(glob.glob(os.path.join(installdir, '*.tar.xz')))
        assert len(tars) == 1, tars
        expect_dir = os.path.basename(tars[0])
        expect_dir = expect_dir[:len(expect_dir) - 7]

        with tarfile.open(tars[0], mode='r:xz') as tar:
            tar.extractall(unpackdir)

        subprocess.check_call(['make', '-C',
            os.path.join(unpackdir, expect_dir), '-s', '-j5'])
        subprocess.check_call(['install', '-s', '-m644',
            os.path.join(unpackdir, expect_dir, 'release', 'game.so'),
            os.path.join(installdir, subdir, 'game.so')])

GAME_DATA_SUBCLASS = Quake2GameData
