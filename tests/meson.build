# Copyright 2022 Simon McVittie
# SPDX-License-Identifier: FSFAP

subdir('data')
subdir('vfs')

make_vfs_zip = [
  python,
  files(meson.project_source_root() / 'tools' / 'make-vfs-zip.py'),
  '@OUTPUT@',
] + test_vfs_files

custom_target(
  'vfs.zip',
  build_by_default : true,
  command : make_vfs_zip,
  input : test_vfs_files,
  output : 'vfs.zip',
)

custom_target(
  'changelog.gz',
  build_by_default : true,
  capture : true,
  command : ['gzip', '-nc9', '@INPUT@'],
  input : meson.project_source_root() / 'debian' / 'changelog',
  output : 'changelog.gz',
  install_dir : pkgdatadir,
)

configure_file(
  copy : true,
  input : meson.project_source_root() / 'data' / 'copyright',
  output : 'copyright',
)

foreach t : [
  'pyflakes',
  'reuse',
]
  test(
    t,
    files(t + '.sh'),
    env : [
      'GDP_BUILDDIR=' + meson.project_build_root(),
      'GDP_PKGDATADIR=' + meson.project_build_root(),
      'GDP_SRCDIR=' + meson.project_source_root(),
      'GDP_UNINSTALLED=1',
      'LC_ALL=C.UTF-8',
      'PYTHONPATH=' + meson.project_source_root(),
    ],
    protocol : 'tap',
    suite : 'lint',
  )
endforeach

foreach t : [
  'deb',
  'hashed_file',
  'integration',
  'rpm',
  'umod',
]
  test(
    t,
    python,
    args : [files(t + '.py')],
    env : [
      'GDP_BUILDDIR=' + meson.project_build_root(),
      'GDP_PKGDATADIR=' + meson.project_build_root(),
      'GDP_SRCDIR=' + meson.project_source_root(),
      'GDP_UNINSTALLED=1',
      'LC_ALL=C.UTF-8',
      'PYTHONPATH=' + meson.project_source_root(),
    ],
    protocol : 'tap',
  )
endforeach

foreach t : [
  'check_syntax',
  'check_equivalence',
]
  test(
    t,
    python,
    args : [files(meson.project_source_root() / 'tools' / t + '.py')],
    env : [
      'GDP_BUILDDIR=' + meson.project_build_root(),
      'GDP_PKGDATADIR=' + meson.project_build_root(),
      'GDP_SRCDIR=' + meson.project_source_root(),
      'GDP_UNINSTALLED=1',
      'LC_ALL=C.UTF-8',
      'PYTHONPATH=' + meson.project_source_root(),
    ],
    suite : 'slow',
    timeout : 150,
  )
endforeach

foreach t : [
  'download_shareware',
]
  test(
    t,
    python,
    args : [files(t + '.py')],
    env : [
      'GDP_BUILDDIR=' + meson.project_build_root(),
      'GDP_PKGDATADIR=' + meson.project_build_root(),
      'GDP_SRCDIR=' + meson.project_source_root(),
      'GDP_UNINSTALLED=1',
      'LC_ALL=C.UTF-8',
      'PYTHONPATH=' + meson.project_source_root(),
    ],
    suite : [
      'network',
      'slow',
    ],
  )
endforeach
