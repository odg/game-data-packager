#!/bin/sh
# Copyright © 2022 Simon McVittie
# SPDX-License-Identifier: GPL-2.0-or-later

if command -v pyflakes3 >/dev/null; then
    pyflakes=pyflakes3
elif command -v pyflakes3k >/dev/null; then
    pyflakes=pyflakes3k
elif command -v python3-pyflakes >/dev/null; then
    pyflakes=python3-pyflakes
elif command -v pyflakes-3 >/dev/null; then
    pyflakes=pyflakes-3
elif [ -x "$(ls -1 /usr/bin/pyflakes-python3.* 2>/dev/null | tail -n 1)" ]; then
    pyflakes="$(ls -1 /usr/bin/pyflakes-python3.* 2>/dev/null | tail -n 1)"
else
    echo "1..0 # SKIP pyflakes3 not found"
    exit 0
fi

cd "$GDP_SRCDIR"

export LC_ALL=C.UTF-8
n=0

for file in \
    game_data_packager/*.py \
    game_data_packager/*/*.py \
    runtime/*.py \
    tests/*.py \
    tools/*.py \
; do
    n=$(( n + 1 ))
    if "$pyflakes" "$file" >&2; then
        echo "ok $n $file"
    else
        echo "not ok $n $file # TODO warnings from pyflakes"
    fi
done

echo "1..$n"
