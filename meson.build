# SPDX-FileCopyrightText: 2022 Simon McVittie
# SPDX-License-Identifier: FSFAP

project(
  'game-data-packager',
  version : '70',
  meson_version : '>= 0.56.0',
  default_options : [
    'warning_level=3',
  ],
)

python = find_program('python3')

gamedatadir = get_option('gamedatadir')

if gamedatadir == ''
  gamedatadir = get_option('datadir')
endif

distro = get_option('distro')
format = get_option('format')
pkgdatadir = gamedatadir / meson.project_name()
bindir = get_option('bindir')
libdir = get_option('libdir')
runtimedir = gamedatadir / meson.project_name() + '-runtime'
program_prefix = get_option('program_prefix')

install_symlinks = []

configure_file(
  input : 'tools/gdp.in',
  output : 'game-data-packager',
  configuration : {
    'pkgdatadir' : get_option('prefix') / pkgdatadir,
  },
  install : true,
  install_dir : bindir,
  install_mode : 'rwxr-xr-x',
)

configure_file(
  input : 'tools/run-uninstalled.in',
  output : 'run-gdp-uninstalled',
  configuration : {
    'srcdir' : meson.project_source_root(),
    'builddir' : meson.project_build_root(),
    'command' : 'python3 -m game_data_packager.command_line',
  },
)

configure_file(
  input : 'tools/run-uninstalled.in',
  output : 'run-tool-uninstalled',
  configuration : {
    'srcdir' : meson.project_source_root(),
    'builddir' : meson.project_build_root(),
    'command' : '',
  },
)

version_py = custom_target(
  'version.py',
  build_by_default : true,
  capture : true,
  command : [
    python,
    files('game_data_packager/version.py'),
    '--distro=' + get_option('distro'),
    '--format=' + get_option('format'),
  ],
  output : 'version.py',
  install : true,
  install_dir : pkgdatadir / 'game_data_packager',
)

configure_file(
  copy : true,
  input : 'data' / 'copyright',
  output : 'copyright',
  install : true,
  install_dir : pkgdatadir,
)

foreach package : ['descent2-demo-data']
  configure_file(
    copy : true,
    input : 'data' / package + '.copyright',
    output : package + '.copyright',
    install : true,
    install_dir : pkgdatadir,
  )
endforeach

foreach package : ['doom-common']
  configure_file(
    copy : true,
    input : 'data' / package + '.preinst.in',
    output : package + '.preinst.in',
    install : true,
    install_dir : pkgdatadir,
  )
endforeach

subdir('data')
subdir('doc')
subdir('game_data_packager')
subdir('icons')
subdir('runtime')

make_vfs_zip = [
  python,
  files('tools/make-vfs-zip.py'),
  '@OUTPUT@',
] + vfs_files

custom_target(
  'vfs.zip',
  build_by_default : true,
  command : make_vfs_zip,
  input : vfs_files,
  output : 'vfs.zip',
  install : true,
  install_dir : pkgdatadir,
)

custom_target(
  'bash_completion',
  build_by_default : true,
  capture : true,
  command : [python, files('tools/bash_completion.py')],
  depend_files : supported_games_yaml,
  output : 'bash_completion',
  install : true,
  install_dir : pkgdatadir,
)

custom_target(
  'changelog.gz',
  build_by_default : true,
  capture : true,
  command : ['gzip', '-nc9', '@INPUT@'],
  input : meson.project_source_root() / 'debian' / 'changelog',
  output : 'changelog.gz',
  install : true,
  install_dir : pkgdatadir,
)

subdir('etc')

foreach game : [
  'doom2-masterlevels',
  'etqw',
  'quake2',
  'quake2-groundzero',
  'quake2-reckoning',
  'quake3',
  'quake3-team-arena',
  'quake3-threewave',
  'quake4',
  'quake-aopfm',
  'quake-armagon',
  'quake',
  'quake-dissolution',
  'quake-dopa',
]
  custom_target(
    game + '.desktop',
    input : 'runtime' / game + '.desktop.in',
    output : game + '.desktop',
    command : [
      'env',
      'PYTHONPATH=' + meson.project_source_root(),
      files(meson.project_source_root() / 'tools' / 'expand_vars.py'),
      '@INPUT@',
      '@OUTPUT@',
    ],
    install : true,
    install_dir : get_option('datadir') / 'applications',
  )
endforeach

foreach game : [
  'unreal',
  'unreal-gold',
  'ut99',
]
  custom_target(
    game + '.desktop',
    input : 'runtime' / game + '.desktop.in',
    output : game + '.desktop',
    command : [
      'env',
      'PYTHONPATH=' + meson.project_source_root(),
      files(meson.project_source_root() / 'tools' / 'expand_vars.py'),
      '@INPUT@',
      '@OUTPUT@',
    ],
    install : true,
    install_dir : runtimedir,
  )
endforeach

foreach game : [
  'etqw-dedicated',
  'etqw',
  'quake2-server',
  'quake2',
  'quake3-server',
  'quake3',
  'quake4-dedicated',
  'quake4',
  'quake-server',
  'quake',
  'unreal-gold',
  'unreal',
  'ut99',
]
  yaml = custom_target(
    'launch-' + game + '.yaml',
    input : 'runtime' / 'launch-' + game + '.yaml.in',
    output : 'launch-' + game + '.yaml',
    command : [
      'env',
      'PYTHONPATH=' + meson.project_source_root(),
      files(meson.project_source_root() / 'tools' / 'expand_vars.py'),
      '@INPUT@',
      '@OUTPUT@',
    ],
  )
  custom_target(
    'launch-' + game + '.json',
    input : yaml,
    output : 'launch-' + game + '.json',
    command : [
      'env',
      'PYTHONPATH=' + meson.project_source_root(),
      files(meson.project_source_root() / 'tools' / 'yaml2json.py'),
      '@INPUT@',
      '@OUTPUT@',
    ],
    install : true,
    install_dir : runtimedir,
  )
endforeach

subdir('tests')

foreach symlink : install_symlinks
  if meson.version().version_compare('>=0.61.0') and not symlink['pointing_to'].startswith('/')
    install_symlink(
      symlink['link_name'],
      install_dir : symlink['install_dir'],
      pointing_to : symlink['pointing_to'],
    )
  else
    meson.add_install_script(
      'tools/meson-compat-install-symlink.sh',
      symlink['link_name'],
      symlink['install_dir'],
      symlink['pointing_to'],
    )
  endif
endforeach
