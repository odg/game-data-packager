Installing Quake II data
========================

Please use game-data-packager to build and install the quake2-full-data
or quake2-demo-data package.

For quake2-demo-data you will need a Quake II CD-ROM, or at least
baseq2/pak0.pak and the baseq2/video directory from a Quake II
installation. game-data-packager can download all other
necessary files. Typical uses:

    game-data-packager -d ~/Downloads quake2 /path/to/search
        Save the generated package to ~/Downloads to install later

    game-data-packager --install quake2 /path/to/search
        Install the generated package, do not save it for later

game-data-packager will automatically search various common installation
paths. If you are installing from a CD-ROM or a non-standard location,
add the installation path or CD-ROM mount point to the command line.

If you have other game files available locally, you can reduce downloading
by adding the path to q2-3.20-x86-full-ctf.exe, or a fully patched version
3.20 such as the one distributed by Steam, to the command line.

For more details please see the output of "game-data-packager quake3 --help"
or the game-data-packager(6) man page.

Demo version
============

For the demo version, run something like this:

    game-data-packager -d ~/Downloads quake2 --demo

and add the path to q2-314-demo-x86.exe to the command line if you
already have it.

Music
=====

For the music, either provide a directory containing pre-ripped
audio named like baseq2/music/02.ogg, xatrix/music/02.ogg or
rogue/music/02.ogg, or use syntax like

    game-data-packager -d ~/Downloads quake2 --package=quake2-music /dev/cdrom

to rip and encode CD audio tracks.

You might be able to download suitable audio files here:
https://steamcommunity.com/app/2320/discussions/0/864974467511619676/

Expansions
==========

If you have the expansions The Reckoning (xatrix) or Ground Zero (rogue),
add their installation directories or CD-ROM mount points to the command line.
