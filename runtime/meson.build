# Copyright 2022 Simon McVittie
# SPDX-License-Identifier: FSFAP

install_data(
  'gdp_launcher_base.py',
  install_dir : runtimedir,
  install_mode : 'rwxr-xr-x',
)

foreach pair : [
  ['quake', 'quake-server'],
  ['quake2', 'quake2-server'],
  ['quake3', 'quake3-server'],
  ['quake4', 'quake4-dedicated'],
]
  install_data(
    'gdp_launcher_base.py',
    install_dir : gamedatadir / pair[0],
    rename : pair[1],
  )
  install_symlinks += {
    'link_name' : program_prefix + pair[0],
    'install_dir' : get_option('bindir'),
    'pointing_to' : get_option('prefix') / runtimedir / 'gdp-launcher',
  }
  install_symlinks += {
    'link_name' : program_prefix + pair[1],
    'install_dir' : get_option('bindir'),
    'pointing_to' : get_option('prefix') / gamedatadir / pair[0] / pair[1],
  }
endforeach

foreach pair : [
  ['etqw', 'etqw-dedicated'],
]
  install_data(
    'gdp_launcher_base.py',
    install_dir : libdir / pair[0],
    rename : pair[1],
  )
  install_symlinks += {
    'link_name' : program_prefix + pair[0],
    'install_dir' : get_option('bindir'),
    'pointing_to' : get_option('prefix') / runtimedir / 'gdp-launcher',
  }
  install_symlinks += {
    'link_name' : program_prefix + pair[1],
    'install_dir' : get_option('bindir'),
    'pointing_to' : get_option('prefix') / libdir / pair[0] / pair[1],
  }
endforeach

install_data(
  'gdp-launcher.py',
  'openurl.py',
  install_dir : runtimedir,
  install_mode : 'rwxr-xr-x',
  rename : [
    'gdp-launcher',
    'gdp-openurl',
  ],
)

install_data(
  'confirm-binary-only.txt',
  'missing-data.txt',
  'quakeX.missing-data.txt',
  install_dir : runtimedir,
)

foreach game : ['unreal', 'ut99']
  install_symlinks += {
    'link_name' : game + '.missing-engine.txt',
    'install_dir' : runtimedir,
    'pointing_to' : 'missing-data.txt',
  }
endforeach

foreach game : ['etqw', 'quake', 'quake2', 'quake3', 'quake4']
  install_symlinks += {
    'link_name' : game + '.missing-data.txt',
    'install_dir' : runtimedir,
    'pointing_to' : 'quakeX.missing-data.txt',
  }
endforeach

foreach game : ['etqw', 'quake4']
  install_symlinks += {
    'link_name' : game + '.missing-engine.txt',
    # TODO: Remove prefix after testing
    'install_dir' : get_option('prefix') / runtimedir,
    'pointing_to' : 'quakeX.missing-data.txt',
  }
endforeach

install_data(
  'doom2-masterlevels.py',
  install_dir : get_option('bindir'),
  install_mode : 'rwxr-xr-x',
  rename : program_prefix + 'doom2-masterlevels',
)

meson.add_install_script(
  meson.project_source_root() / 'tools' / 'meson-install-multiple-copies.sh',
  version_py,
  '644',
  pkgdatadir / 'game_data_packager' / 'gdp_launcher_version.py',
  runtimedir / 'gdp_launcher_version.py',
  gamedatadir / 'quake' / 'gdp_launcher_version.py',
  gamedatadir / 'quake2' / 'gdp_launcher_version.py',
  gamedatadir / 'quake3' / 'gdp_launcher_version.py',
  gamedatadir / 'quake4' / 'gdp_launcher_version.py',
  libdir / 'etqw' / 'gdp_launcher_version.py',
)
